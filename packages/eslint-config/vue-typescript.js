/** @type{ import("eslint").Linter.BaseConfig } */
module.exports = {
	parserOptions: {
		parser: '@typescript-eslint/parser',
		extraFileExtensions: ['.vue'],
	},
};
