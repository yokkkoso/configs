const prettier = require('eslint-config-prettier');

/** @type{ import("eslint").Linter.BaseConfig } */
module.exports = {
	rules: {
		...prettier.rules,

		'arrow-body-style': 0,
	},
};
