/** @type{ import("eslint").Linter.BaseConfig } */
module.exports = {
	rules: {
		'unicorn/prefer-module': 2,
	},
};
